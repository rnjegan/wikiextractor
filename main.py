#!/usr/bin/python3
# -*- coding: utf-8 -*

"""Wikipedia Summary & Content Extractor

Changes in processpages.wikiextractor.py and processpages.idextractor.py might be necessary

adapted from https://stackoverflow.com/questions/23351103/extract-the-main-article-text-from-a-wikipedia-page-using-python
"""

from SPARQLWrapper.SPARQLExceptions import EndPointInternalError
from SPARQLWrapper import SPARQLWrapper, JSON
from urllib.error import URLError
import os
import threading
import time
import yaml

from processpages.wikiextractor import handle_page_ids
from processpages.wikiextractor import split_page_ids
from processpages.wikiextractor import initiate_content_retrieval

start_time = time.time()

with open ("config.yml", "r") as file:
    yml_reader = yaml.safe_load(file)

count_query = yml_reader["count_query"]
extract_query = yml_reader["extract_query"]
document_length = yml_reader["document_length"]

print("Collecting pages and urls...")
thread = threading.Thread(target=handle_page_ids(count_query, extract_query))
thread.start()
thread.join()
print("Collecting pages and urls finished.")

print("Splitting pages for easier processing...")
thread = threading.Thread(target=split_page_ids())
thread.start()
thread.join()
print("Splitting pages for easier processing finished.")

print("Retrieving page contents...")
thread = threading.Thread(target=initiate_content_retrieval(document_length))
thread.start()
thread.join()
print("Retrieving page contents finished.")

elapsed_time = (time.time() - start_time)
print("Script took " + str(elapsed_time) + "seconds.")
