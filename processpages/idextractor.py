#!/usr/bin/python3
# -*- coding: utf-8 -*

"""Wikipedia Title & DBPedia Page and Link Extractor

Uses SPARQL queries to extract all unique titles and DBPedia URLs from all DBPedia objects with the tag 'person'.
Saves the result in the file 'data/pageLinks.csv'

"""

import threading
import os
from urllib.error import URLError

from SPARQLWrapper import SPARQLWrapper, JSON
from SPARQLWrapper.SPARQLExceptions import EndPointInternalError

file_name_ids = "data/pageLinks.csv"
sparql = SPARQLWrapper("http://dbpedia.org/sparql")
count = 0
thread_lock = threading.Lock()
current_count_query = ""
current_extract_query = ""

# one thread, that downloads all assigned titles and urls
class MyThread(threading.Thread):
    def __init__(self, thread_id):
        threading.Thread.__init__(self)
        self.thread_id = thread_id

    def run(self):
        print("Worker No. " + self.thread_id + " was started.")
        index = int(self.thread_id)
        # set the base number, where the respective thread needs to start
        # base 0 starts with the first page
        # in all other cases: increment by 100000, since one thread processes 100000 pages
        if index == 0:
            base = 0
        else:
            base = 100000 * index
        # set the number of iterations a single thread has to process to 10
        for i in range(0, 9):
            result_array = []
            retries = 0
            while retries < 10:
                # start at page zero if first iteration
                if i == 0:
                    result_array = execute_sparql_query(
                        self.thread_id, base, 0)
                # else at page 10000 for the second iteration, page 20000 for the third, and so on
                else:
                    result_array = execute_sparql_query(
                        self.thread_id, base, i * 10000)
                if result_array is None:
                    print('Thread: ' + self.thread_id +
                          '; Repeat cycle due to results being empty: ' + str(i+1))
                    retries += 1
                else:
                    break
            thread_lock.acquire()
            with open(file_name_ids, "a", encoding="utf8") as f:
                for page_title_and_id in result_array:
                    if page_title_and_id is None:
                        thread_lock.release()
                        continue
                    f.write(page_title_and_id + "\n")
            thread_lock.release()
            print('Thread: ' + self.thread_id +
                  '; Cycle processed: ' + str(i+1))


# get the number of all pages with person tag and save in "count"
def count_pages():
    sparql.setQuery(current_count_query)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()
    global count
    count = results["results"]["bindings"][0]["callret-0"]["value"]


# executing the SPARQL Query to get all titles and urls. crawling over all results with person tag requires threads
# and offsets
# base: between 0 and 9, so that one thread only has to process 100000 pages, will be fixed by variable
# increment: iteration of the thread. since one SPARQL call can only return up to 10000 results
def execute_sparql_query(thread_name, base, increment):
    sparql_results = {}
    sparql.setQuery(current_extract_query + str(base + increment))
    sparql.setReturnFormat(JSON)
    retries = 0
    if retries < 10:
        try:
            sparql_results = sparql.query().convert()
            retries += 1
        except URLError:
            execute_sparql_query(thread_name, base, increment)
        except EndPointInternalError:
            execute_sparql_query(thread_name, base, increment)
        result_array = []
        try:
            counter = 0
            for result in sparql_results["results"]["bindings"]:
                counter += 1
                if result is None:
                    continue
                else:
                    result_array.append(
                        result["label"]["value"] + ',' + result["person"]["value"])
            if result_array is None:
                print('Thread: ' + thread_name +
                      '; repeat Cycle due to result_array == None: ' + str(increment))
                execute_sparql_query(thread_name, base, increment)
            else:
                return result_array
        except TypeError:
            print('Thread: ' + thread_name +
                  '; Skipped cycle due to TypeError: ' + str(increment))
            execute_sparql_query(thread_name, base, increment)


# return the number of pages with tag "person"
def get_number_of_pages(count_query, extract_query):
    global current_count_query
    current_count_query = count_query
    global current_extract_query
    current_extract_query = extract_query
    thread = threading.Thread(target=count_pages())
    thread.start()
    thread.join()
    print('Number of objects with tag: ' + str(count))
    return


# use threading to extract all links corresponding to pages with tag "person" and save to disc
def extract_page_ids():
    threads = []
    # remove old file with page ids and links to prevent duplicates
    if (os.path.exists(file_name_ids)):
        os.remove(file_name_ids)

    # determine number of threads, one thread processes 100.000 pages
    # divided by number of pages. plus one, since thread numbers start at 0
    if int(count) < 100000:
        number_of_threads = 1
    else:
        number_of_threads = int(int(count) / 100000) + 1
    for digit in [format(x, 'd') for x in range(number_of_threads)]:
        thread = MyThread(digit)
        thread.start()
        # if the same order in the pageLinks file is necessary for every execution of the script, uncomment the following line
        # thread.join()
        threads.append(thread)

    for t in threads:
        t.join()
    print("Saved all IDs to disc")
