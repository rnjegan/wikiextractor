#!/usr/bin/python3
# -*- coding: utf-8 -*

"""Wikipedia Summary & Content Extractor

Downloads summary or full content version of Wikipedia articles and saves to txt file in directory "data"
Change flag summary in line 28 to switch between summary and content

adapted from https://stackoverflow.com/questions/23351103/extract-the-main-article-text-from-a-wikipedia-page-using-python
"""

import re
import threading
import time
import os

import requests
import wikipedia
import json.decoder
import urllib3.exceptions
from processpages.idextractor import get_number_of_pages, extract_page_ids

url = 'https://en.wikipedia.org/wiki/'
file_path = "data/"
file_name_ids = "data/pageLinks.csv"
file_name_summary = "data/fullTextSummary.tsv"
file_name_full_content = "data/fullTextContent.tsv"

# # set summary to False to extract full content version of files
# summary = False

# If True, then Summary will be crawled, if False, then full content
summary = True

number_of_threads = 10
thread_lock = threading.Lock()
result_array = []


def handle_page_ids(count_query, extract_query):
    get_number_of_pages(count_query, extract_query)
    # create data directory if it does not exist
    if not os.path.exists(file_path):
        os.makedirs(file_path)
    extract_page_ids()

def write_to_file(file_name, result_string):
    thread_lock.acquire()
    with open(file_name, "a", encoding="utf8") as f:
        f.write(result_string)
    thread_lock.release()

class MyThread(threading.Thread):
    def __init__(self, thread_id):
        threading.Thread.__init__(self)
        self.thread_id = thread_id

    def run(self):
        print("Worker No. " + self.thread_id + " was started.")
        start_time = time.time()
        counter = 1
        result_string = ''
        for line in result_array[int(self.thread_id)]:
            try:
                result_string += download_page_content(line.split(",")[0], 0)
            except TypeError:
                print('TyperError for: ' + line)
            # only write after 50 pages have been processed -> speed increase
            if counter % 50 == 0:
                if summary:
                    write_to_file(file_name_summary, result_string)
                else:
                    write_to_file(file_name_full_content, result_string)
                print('Thread: ' + self.thread_id + '; Pages processed: ' + str(
                    counter) + '; Pages per second: ' + str(counter / (time.time() - start_time)))
                result_string = ""
            counter += 1
        # write remaining results to file, or if fewer than 50 pages have been processed
        if summary:
            write_to_file(file_name_summary, result_string)
        else:
            write_to_file(file_name_full_content, result_string)
        print('Thread: ' + self.thread_id + '; Pages processed: ' + str(
            counter) + '; Pages per second: ' + str(counter / (time.time() - start_time)))


# retrieve the content of one page, including retries in case of errors
def download_page_content(page_title, retries):
    result = ""
    try:
        page = wikipedia.page(page_title, auto_suggest=False)
        if summary:
            page_data = page.summary
        else:
            page_data = page.content
        page_data = re.compile(r'\n').sub("", page_data)
        page_data = re.compile(r'\t').sub("", page_data)
        result = page_title + '\t' + page.url + '\t' + str(page.links) + '\t' + str(page.categories) + '\t' + page_data + '\n'
    # error handling for various exceptions, often with retry count of up to 3, then the url is ignored
    except wikipedia.exceptions.PageError:
        if retries > 3:
            print("Error for URL: " + url +
                  str(page_title) + " , exceeded retries.")
            return ''
        else:
            print("Error for URL: " + url +
                  str(page_title) + " , trying again...")
            retries += 1
            time.sleep(0.5)
            download_page_content(page_title, retries)

    except requests.exceptions.ConnectionError:
        if retries > 3:
            print("Error for URL: " + url +
                  str(page_title) + " , exceeded retries.")
            return ''
        else:
            print("Error for URL: " + url +
                  str(page_title) + " , trying again...")
            retries += 1
            time.sleep(0.5)
            download_page_content(page_title, retries)
    except wikipedia.exceptions.DisambiguationError:
        print("Non-unique URL will be skipped: " + url + str(page_title))
        return ''
    except json.decoder.JSONDecodeError:
        print("Returned JSON was None, URL will be skipped: " + url + str(page_title))
        return ''
    except requests.exceptions.ConnectionError:
        print("Max retries reached, skipping URL: " + url + str(page_title))
        return ''
    except urllib3.exceptions.MaxRetryError:
        print("Max retries reached, skipping URL: " + url + str(page_title))
        return ''
    except UnicodeEncodeError:
        print("UniCodeEncodeError, skipping URL: " + url + str(page_title))
        return ''
    except TypeError:
        print("TypeError, skipping URL: " + url + str(page_title))
        return ''
    return result


# split the file containing all ids & urls for easier processing during page content retrieval
def split_page_ids():
    number_of_lines = len(open(file_name_ids, encoding="utf8").readlines())
    global result_array
    with open(file_name_ids, "r", encoding="utf8") as f:
        line_counter = 0
        current_array = []
        for line in f:
            current_array.append(line)
            line_counter += 1
            # split in parts according to the number of threads
            if line_counter % (int(number_of_lines / number_of_threads)) == 0:
                result_array.append(current_array[:])
                current_array.clear()
                print("Processed " + str(line_counter) +
                      " lines, size of result_array: " + str(len(result_array)))


# start threads and the retrieval process
def initiate_content_retrieval(document_length):
    # prepare for specified number of threads
    global summary
    summary = document_length
    thread_list = ["{:01d}".format(x) for x in range(number_of_threads)]
    threads = []

    # start all threads
    for digit in thread_list:
        thread = MyThread(digit)
        thread.start()
        threads.append(thread)

    # wait for all threads to finish
    for t in threads:
        t.join()
        print('Thread No. ' + str(t.thread_id) + ' is shut down.')
    print("Exiting Main Thread")
