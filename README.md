# Wiki Extractor

## Requirements
To install the required packages and libraries please execute:
```
pip install -r requirements.txt
```


## Usage
"main.py" can be executed and starts the retrieval of all pages with the required DBPedia tags, saves all links to the corresponding Wikipedia pages and saves the text content to the "data" directory.
```
python main.py
```

The default tag used in the files is the "person" tag, which states that the Wikipedia pages should be classified in the DBPedia ontology as a "person", see here http://dbpedia.org/ontology/Person

To use other filters or tags, edit two queries in the "config.yml" file.

They have to be valid SPARQL expressions, which can be checked, for example, in the Virtuoso SPARQL Query Editor, http://dbpedia.org/sparql
The "count_query" retrieves the number of pages that fit the required tag or filter, while the "extract_query" retrieves all pages that fit the criteria. This will probably have to be done in chunks due to size limitations for a single query, which is done in SPARQL via the LIMIT and OFFSET parameters.

The output of the script is saved in the directory "data". All pages, with page title and DBPedia link, are saved in "data/pageLinks.csv". The content of the pages is saved in "data/fullTextContent.tsv" and/or "data/fullTextSummary.tsv". A page is saved in the following way. 
1. The Page Title
2. The Link to the Wikipedia Page
3. All wikipedia pages, that are linked from the current page (here no URLs are saved, but the title of linked wikipedia page), as a list of strings.
4. all categories the page is associated with, as a list of strings. 
5. The text content of the page, which can be the full text content or the summary of the page, depending on which variable is set "config.yml" as "document_length"

This variant saves only the summary (edit in "config.yml"):
```
document_length: !!bool true
```

This variant saves the full text content(edit in "config.yml"):
```
document_length: !!bool false
```

A short example (with the summary variant):
Bhasmasur	https://en.wikipedia.org/wiki/Bhasmasura	['Asura', 'Avatar', 'Daitya', 'Hindu mythology', 'Hindu texts', 'ISBN (identifier)', 'Mohini', 'Puranas', 'Ramakien', 'Sanskrit language', 'Shiva', 'Vishnu', 'Yakshagana']	['All Wikipedia articles written in Indian English', 'Articles containing Sanskrit-language text', 'Articles having same image on Wikidata and Wikipedia', 'Articles with short description', 'Asura', 'Short description matches Wikidata', 'Use Indian English from April 2019']	In Hinduism, Bhasmasura (Sanskrit: भस्मासुर, Bhasmāsura) is an asura or demon, who was granted the power to burn up and immediately turn into ashes (bhasma) anyone whose head he touched with his hand. The asura was tricked by the Vishnu's only female avatar, the enchantress Mohini, to turn himself into ashes.

## SPARQL Queries
Example SPARQL Queries:
Use these queries in "config.yml".

### Fictional Characters
To retrieve all pages that are tagged as "FictionalCharacter". This is the version that is used by default.
Will probably run for 30min.

count_query = """PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> select count(*) { ?person a dbo:FictionalCharacter ; rdfs:label ?
label FILTER(LANG(?label) = "en")}"""

extract_query = """PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> select * { ?person a dbo:FictionalCharacter ; rdfs:label ?label FILTER(LANG(?label) = "en")} LIMIT 10000 OFFSET """

### Person
To retrieve all pages that are tagged as "Person". 
Warning: This will take a long time (usually more than one day), over 1 Million pages are retrieved.

count_query = """PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> select count(*) { ?person a dbo:Person ; rdfs:label ?label FILTER(LANG(?label) = "en")}"""

extract_query = """PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> select * { ?person a dbo:Person ; rdfs:label ?label FILTER(LANG(?label) = "en")} LIMIT 10000 OFFSET """
